import React, {Component} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  FlatList,
  Dimensions,
  RefreshControl,
  ScrollView,
} from 'react-native';
import axios from 'axios';
import Icon from 'react-native-vector-icons/AntDesign';
import moment from 'moment';
import 'moment/locale/id';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import NetInfo from '@react-native-community/netinfo';
import Genres from '../Genres';
import {Image} from 'react-native-elements';

export default class ListMovies extends Component {
  NetInfoSubscription = null;

  constructor(props) {
    super(props);
    this.state = {
      dataMovies: [],
      isLoading: false,
      connectionStatus: false,
    };
  }

// Internet Connection Checker  
  componentDidMount = () => {
    this.NetInfoSubscription = NetInfo.addEventListener(
      this._handleConnectivityChange,
    );
    this.getDataMovies();
  };

  componentWillUnmount() {
    this.NetInfoSubscription && this.NetInfoSubscription();
  }

  _handleConnectivityChange = state => {
    this.setState({connectionStatus: state.isConnected});
  };

// Fetching data movies  
  getDataMovies = async () => {
    this.setState({isLoading: true});
    const url = 'http://code.aldipee.com/api/v1/movies/';
    const getMovies = await axios.get(url);
    this.setState({dataMovies: getMovies.data.results});
    this.setState({isLoading: false});
  };

// Menampilkan rekomendasi film dan navigasi ke detail film ketika diklik  
  rekomendasiMovie = ({item}) => {
    return (
      <TouchableOpacity
        activeOpacity={0.6}
        onPress={() => this.props.navigation.navigate('MovieDetailScreen', item.id)}
        style={Styles.rekomendasiFilm}>
        <Image
          source={{uri: item.poster_path}}
          style={{height: 200, width: 110, borderRadius: 20}}
        />
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <>
        {this.state.isLoading == true ? (
          <View style={{flex: 1, zIndex: 100, backgroundColor: '#22252a'}}>
            <SkeletonPlaceholder
              backgroundColor={'#2d3035'}
              highlightColor={'#4d5259'}>
              <View style={Styles.recommend} />
              <View style={{flexDirection: 'row'}}>
                <View style={Styles.poster} />
                <View style={Styles.poster} />
                <View style={Styles.poster} />
                <View style={Styles.poster} />
              </View>
              <View style={Styles.latest} />
              <View style={{flexDirection: 'row'}}>
                <View style={Styles.poster} />
                <View style={{flexDirection: 'column'}}>
                  <View style={Styles.title} />
                  <View style={Styles.info} />
                  <View style={Styles.info} />
                  <View style={Styles.info} />
                  <View style={Styles.info} />
                </View>  
              </View>
              <View style={{flexDirection: 'row', marginTop: 20}}>
                <View style={Styles.poster} />
                <View style={{flexDirection: 'column'}}>
                  <View style={Styles.title} />
                  <View style={Styles.info} />
                  <View style={Styles.info} />
                  <View style={Styles.info} />
                  <View style={Styles.info} />
                </View>
              </View>
            </SkeletonPlaceholder>
          </View>
        ) : (
          <View style={Styles.container}>

{/* Refresh control home screen */}
            <ScrollView
              refreshControl={
                <RefreshControl
                  refreshing={this.state.isLoading}
                  onRefresh={this.getDataMovies}
                />
              }>
                
{/* Pengecekan koneksi internet */}
              {this.state.connectionStatus ? (
                <></>
              ) : (
                <View style={Styles.connection}>
                  <Text style={Styles.offline}>
                    You are OFFLINE. Please check your internet connection!
                  </Text>
                </View>
              )}

{/*
    Data tampilan Recommended.
    Menampilkan poster film yang diurutkan dari vote average tertinggi ke terendah.
*/}

              <Text style={Styles.headerText}>Recommended</Text>
              <View>
                <FlatList
                  horizontal={true}
                  showsHorizontalScrollIndicator={false}
                  data={this.state.dataMovies
                    .sort((x, y) => {
                      return y.vote_average - x.vote_average;
                    })
                    .slice(0, 10)}
                  renderItem={this.rekomendasiMovie}
                  keyExtractor={(i, k) => k.toString()}
                />
              </View>

{/*
    Data tampilan Latest Update.
    Menampilkan poster, judul, tanggal rilis, rating, genre, dan tombol show more.
    Ketika tombol show more diklik makan akan pindah ke halaman movie detail screen.
    Tampilan diurutkan dari tanggal rilis awal ke terakhir.
*/}

              <Text style={Styles.headerText}>Latest Upload</Text>
              <View style={{flex: 1}}>
                {this.state.dataMovies
                  .sort((x, y) => {
                    return new Date(x.release_date) - new Date(y.release_date);
                  })
                  .map(item => (
                    <View key={item.id} style={Styles.movieContainer}>
                      <Image
                        source={{uri: item.poster_path}}
                        style={{height: 192, width: 108, borderRadius: 20}}
                      />
                      <View style={Styles.infoContainer}>
{/* Menampilkan judul film */}
                        <View style={{marginBottom: 5}}>
                          <Text style={Styles.textJudul}>
                            {item.original_title}
                          </Text>
                        </View>
{/* Menampilkan tanggal rilis film */}
                        <Text
                          style={Styles.textRilisFilm}>
                          {item.release_date == null ? 'Coming Soon' : 
                              moment(item.release_date).format('Do MMM YYYY')}
                        </Text>
{/* Menampilkan rating film */}
                        <View
                          style={Styles.textRatingFilm}>
                          {item.vote_average == 0 ? (
                            <Text
                              style={Styles.textRated}>
                              Not Rated Yet
                            </Text>
                          ) : (
                            <Text
                              style={Styles.textRated}>
                              <Icon name="star" size={14} color="#FFDF00" />{' '}
                              {item.vote_average}
                            </Text>
                          )}
                        </View>
{/* Menampilkan genre film */}
                        <Text
                          style={Styles.textGenreFilm}>
                          <Genres id={item.id} />
                        </Text>
{/* Menampilkan tombol show more */}
                        <TouchableOpacity
                          activeOpacity={0.6}
                          onPress={() =>
                            this.props.navigation.navigate('MovieDetailScreen', item.id)
                          }
                          style={Styles.showMore}>
                          <Text style={Styles.textShowMore}>
                            Show More
                          </Text>
                        </TouchableOpacity>
                        
                      </View>
                    </View>
                  ))}
              </View>
            </ScrollView>
          </View>
        )}
      </>
    );
  }
}

const winWidth = Dimensions.get('window').width;
const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#22252a',
  },
  connection: {
    marginTop: 10,
    borderRadius: 20,
    backgroundColor: '#b12318',
    width: '33%',
    alignSelf: 'center',
    padding: 3,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  offline: {
    textAlign: 'center',
    justifyContent: 'center',
    color: '#fff',
    marginLeft: 10,
  },
  headerText: {
    fontSize: 22,
    color: 'white',
    marginLeft: 13,
    marginTop: 15,
    fontFamily: 'Nunito-Bold',
  },
  logo: {
    marginTop: 20,
    marginLeft: 15,
    height: 40,
    width: 108,
    borderRadius: 7,
  },
  recommend: {
    marginTop: 20,
    marginLeft: 15,
    height: 25,
    width: 158,
    marginBottom: 15,
    borderRadius: 7,
  },
  poster: {
    height: 192,
    width: 108,
    marginHorizontal: 10,
    borderRadius: 20,
  },
  latest: {
    marginTop: 30,
    marginLeft: 15,
    height: 25,
    width: 143,
    marginBottom: 15,
    borderRadius: 7,
  },
  movieContainer: {
    flexDirection: 'row',
    padding: 10,
    alignItems: 'center',
  },
  infoContainer: {
    marginLeft: 20,
    alignItems: 'flex-start',
    flexWrap: 'wrap',
    width: winWidth * 0.7,
  },
  title: {
    marginTop: 30,
    marginLeft: 15,
    height: 25,
    width: 143,
    marginBottom: 10,
    borderRadius: 5,
  },
  info: {
    marginLeft: 15,
    height: 20,
    width: 103,
    marginBottom: 7,
    borderRadius: 5,
  },
  showMore: {
    backgroundColor: '#3e4249',
    alignSelf: 'flex-start',
    paddingHorizontal: 5,
    paddingVertical: 3,
    borderRadius: 10,
  },
  textShowMore: {
    color: '#fff',
    fontFamily: 'Nunito-Bold',
    fontSize: 14,
  },
  rekomendasiFilm: {
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  textJudul: {
    color: '#fff',
    fontFamily: 'Nunito-Bold',
    fontSize: 18,
  },
  textRilisFilm: {
    color: '#fff',
    fontFamily: 'Nunito-Regular',
    fontSize: 12,
    marginBottom: 5,
  },
  textRatingFilm: {
    paddingVertical: 3,
    paddingHorizontal: 5,
    alignSelf: 'flex-start',
    backgroundColor: '#3e4249',
    borderRadius: 7,
  },
  textRated: {
    color: '#fff',
    fontFamily: 'Nunito-Bold',
    fontSize: 12,
  },
  textGenreFilm: {
    color: '#fff',
    fontFamily: 'Nunito-Bold',
    fontSize: 17,
    marginBottom: 15,
  },
});
