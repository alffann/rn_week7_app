import React, {Component} from 'react';
import {Text, View} from 'react-native';

export default class Splash extends Component {
  componentDidMount() {
    setTimeout(() => {
      this.props.navigation.replace('HomeScreen');
    }, 3000);
  }
  render() {
    return (
      <View
        style={{
          backgroundColor: 'black',
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontFamily: 'Nunito-Bold',
              fontSize: 30,
              color: 'white',
            }}>
            {' '}Splash Icon{' '}
          </Text>
        </View>
      </View>
    );
  }
}

