import React, {Component} from 'react';
import {
  Text,
  View,
  ScrollView,
  Dimensions,
  ImageBackground,
  ActivityIndicator,
  RefreshControl,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import axios from 'axios';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {Image} from 'react-native-elements';
import Icon from 'react-native-vector-icons/AntDesign';
import moment from 'moment';
import 'moment/locale/id';

export default class DetailMovies extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      title: '',
      tagline: '',
      status: '',
      releaseDate: '',
      overview: '',
      poster: null,
      backdrop: null,
      voteAverage: 0,
      runtime: 0,
      genres: [],
      cast: [],
    };
  }

  componentDidMount = () => {
    this.getMovieDetail();
  };

// Fetching data detail movies  
  getMovieDetail = async () => {
    this.setState({isLoading: true});
    const url = 'http://code.aldipee.com/api/v1/movies/';
    const movie = await axios.get(url + this.props.route.params);
    this.setState({
      title: movie.data.original_title,
      tagline: movie.data.tagline,
      status: movie.data.status,
      cast: movie.data.credits.cast,
      genres: movie.data.genres,
      runtime: movie.data.runtime,
      overview: movie.data.overview,
      voteAverage: movie.data.vote_average,
      releaseDate: movie.data.release_date,
      poster: movie.data.poster_path,
      backdrop: movie.data.backdrop_path,
    });
    this.setState({isLoading: false});
  };

// Menampilkan durasi film dalam menit  
  Runtime = () => {
    const m = this.state.runtime;
    return (m + ' minutes').toString();
  };

  render() {
    const winWidth = Dimensions.get('window').width;
    const winHeight = Dimensions.get('window').height;
    return (
      <>
        {this.state.isLoading == true ? (
          <View style={{flex: 1, zIndex: 100, backgroundColor: '#22252a'}}>
            <SkeletonPlaceholder
              backgroundColor={'#2d3035'}
              highlightColor={'#4d5259'}>
              <View style={{alignItems: 'center'}}>
                <View style={Styles.backdrop} />
                <View style={Styles.poster} />
                <View style={Styles.title} />
                <View style={Styles.tagline} />
                <View style={Styles.info} />
                <View style={Styles.info} />
              </View>
              <View style={Styles.genre} />
              <View style={{flexDirection: 'row'}}>
                <View style={Styles.genre1} />
                <View style={Styles.genre2} />
                <View style={Styles.genre2} />
              </View>
              <View style={{alignItems: 'center'}}>
                <View style={Styles.overview} />
              </View>
              <View style={Styles.genre} />
              <View style={{flexDirection: 'row', marginHorizontal: 20}}>
                <View style={Styles.cast} />
                <View style={Styles.cast} />
                <View style={Styles.cast} />
                <View style={Styles.cast} />
                <View style={Styles.cast} />
                <View style={Styles.cast} />
              </View>
            </SkeletonPlaceholder>
          </View>
        ) : (
          <>
{/* Refresh control movie detail screen */}
            <ScrollView
              style={{flexGrow: 1}}
              refreshControl={
                <RefreshControl
                  refreshing={this.state.isLoading}
                  onRefresh={this.getDataMovies}
                />
              }>
{/* Menampilkan backdrop film */}
              <ImageBackground
                PlaceholderContent={<ActivityIndicator />}
                source={{uri: this.state.backdrop}}
                style={{
                  width: winWidth,
                  height: winHeight * 0.3,
                  position: 'absolute',
                }}
              />
              <View style={Styles.container}>
{/* Menampilkan poster film */}
                <View
                  style={{
                    alignItems: 'center',
                    marginTop: -100,
                  }}>
                  <Image
                    source={{uri: this.state.poster}}
                    style={Styles.imgPoster}
                  />
                </View>

{/*
    Data header movie.
    Menampilkan data judul, tagline, status and release date, vote average, & runtime
    dari sebuah film.
*/}

                <View>
                  <Text style={Styles.textTitle}>{this.state.title}</Text>
                  <Text style={Styles.textTagline}>{this.state.tagline}</Text>                 
                  <View style={Styles.infoContainer}>
{/* Data kapan film dirilis/ditayangkan */}
                    <View style={{
                          flexDirection: 'row'
                          }}>
                    <Text style={Styles.textInfo}>
                      <>
                        {'  '}
                        <Text />
                      </>
                      {this.state.status}
                    </Text>
                    <Text style={Styles.textInfo}>
                      <>
                        {'  '}
                        <Text />
                      </>
                      {moment(this.state.releaseDate).format('Do MMMM YYYY')}
                      <>
                        {' '}
                        <Text />
                      </>{' '}
                      <> </>
                    </Text>
                    </View>  
{/* Data vote average film */}
                    <View style={{
                          flexDirection: 'row'
                          }}>
                    <View style={{
                          flexDirection: 'row'
                          }}>       
                    <Icon name="star" color="#FFDF00" size={14} />
                    <Text style={Styles.avgVote}>
                      {this.state.voteAverage}
                      <> </> &#8226;
                    </Text>
                    </View> 
{/* Data runtime/lama film */}
                    <Text style={Styles.textInfo}>
                      <>
                        {' '}
                        <Text />
                      </>
                      {this.Runtime()}
                    </Text>
                    </View>
                  </View>

{/*
    Data genres.
    Menampilkan list genre dari sebuah film.
*/}

                  <View style={{
                        marginTop: 20,
                        marginLeft: 20,
                    }}>
                    <Text style={Styles.textHeader}>Genres</Text>
                    <Text>
                      {this.state.genres.map(genre => (
                        <TouchableOpacity
                          style={{paddingRight: 15, paddingTop: 7}}
                          key={genre.id}>
                          <Text style={Styles.genreName}>{genre.name}</Text>
                        </TouchableOpacity>
                      ))}
                    </Text>
                  </View>

{/*
    Data sinopsis.
    Menampilkan detail dari sinopsis/overview.
*/}

                  <View style={{
                        marginTop: 20,
                        marginLeft: 20,
                    }}>
                    <Text style={Styles.textHeader}>Synopshis</Text>
                    <View style={{marginRight: 20}}>
                      <Text style={Styles.textOverview}>
                        {this.state.overview}
                      </Text>
                    </View>
                  </View>

{/*
    Data aktor dan artis.
    Menampilkan foto aktor dan artis.
    Menampilkan nama aktor dan artis di bawahnya.
*/}
                  <View style={{
                        marginTop: 20,
                        marginLeft: 20,
                    }}>
                    <Text style={Styles.textHeader}>Actors/Artists</Text>
                    <View>
                      <Text style={{alignItems: 'stretch'}}>
                        {this.state.cast
                          .map(item => (
                            <TouchableOpacity
                              key={item.id}
                              style={{
                                height: 200,
                              }}>
                              <Image
                                source={{uri: item.profile_path}}
                                style={Styles.castImg}
                              />
                              <Text style={Styles.castName}>{item.name}</Text>
                            </TouchableOpacity>
                          ))
                          .slice(0, 6)}
                      </Text>
                    </View>
                  </View>

                </View>
              </View>
            </ScrollView>
          </>
        )}
      </>
    );
  }
}

const winWidth = Dimensions.get('window').width;
const Styles = StyleSheet.create({
  container: {
    backgroundColor: '#22252a',
    marginTop: 180,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
  },
  imgPoster: {
    width: 118,
    height: 202,
    borderRadius: 20,
  },
  infoContainer: {
    flexDirection: 'column',
    marginTop: 7,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textTitle: {
    fontFamily: 'Nunito-Bold',
    fontSize: 18,
    color: '#fff',
    textAlign: 'center',
  },
  textTagline: {
    fontFamily: 'Nunito-Regular',
    fontSize: 14,
    color: '#fff',
    textAlign: 'center',
  },
  textHeader: {
    color: '#fff',
    fontSize: 18,
    fontFamily: 'Nunito-Bold',
  },
  textInfo: {
    color: '#fff',
    fontSize: 12,
    fontFamily: 'Nunito-Regular',
  },
  avgVote: {
    textAlign: 'center',
    fontFamily: 'Nunito-Regular',
    color: '#fff',
    fontSize: 12,
    marginLeft: 3,
  },
  genreName: {
    backgroundColor: '#3e4249',
    paddingHorizontal: 6,
    paddingVertical: 3,
    borderRadius: 10,
    fontFamily: 'Nunito-Regular',
    color: '#fff',
    fontSize: 12,
  },
  textOverview: {
    textAlign: 'justify',
    fontFamily: 'Nunito-Regular',
    color: '#fff',
  },
  castImg: {
    height: 120,
    width: 92,
    borderRadius: 10,
    margin: 15,
    alignItems: 'center',
  },
  castName: {
    color: '#fff',
    fontSize: 12,
    fontFamily: 'Nunito-Regular',
    alignSelf: 'center',
    textAlign: 'center',
    width: 62,
    marginBottom: 10,
  },
  showAll: {
    color: '#fff',
    fontSize: 12,
    fontFamily: 'Nunito-Regular',
    backgroundColor: '#3e4249',
    alignSelf: 'center',
    paddingVertical: 4,
    paddingHorizontal: 7,
    borderRadius: 15,
  },
  bsCasts: {
    color: '#fff',
    fontSize: 18,
    fontFamily: 'Nunito-Bold',
    textAlign: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  bsContainer: {
    flexDirection: 'row',
    flex: 1,
    flexWrap: 'wrap',
    justifyContent: 'center',
  },
  bsPoster: {
    height: 98,
    width: 62,
    borderRadius: 15,
    margin: 5,
  },
  bsName: {
    color: '#fff',
    fontSize: 12,
    fontFamily: 'Nunito-Regular',
    alignSelf: 'center',
    textAlign: 'center',
    width: 62,
    marginBottom: 10,
  },
  backdrop: {
    width: winWidth,
    height: 180,
  },
  poster: {
    width: 118,
    height: 92,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  title: {
    marginTop: 6,
    width: 118,
    height: 25,
    borderRadius: 4,
  },
  tagline: {
    marginTop: 6,
    width: 148,
    height: 20,
    borderRadius: 4,
  },
  info: {
    marginTop: 6,
    width: 150,
    height: 15,
    borderRadius: 4,
  },
  genre: {
    marginTop: 20,
    marginLeft: 20,
    width: 55,
    height: 20,
    borderRadius: 5,
  },
  genre1: {
    marginTop: 10,
    marginLeft: 20,
    width: 70,
    height: 18,
    borderRadius: 10,
  },
  genre2: {
    marginTop: 10,
    marginLeft: 5,
    width: 70,
    height: 18,
    borderRadius: 10,
  },
  overview: {
    marginTop: 40,
    width: winWidth * 0.9,
    height: 130,
  },
  cast: {
    marginTop: 10,
    marginLeft: 30,
    width: 92,
    height: 120,
    borderRadius: 10,
  },
});
