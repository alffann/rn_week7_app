import React, {Component} from 'react';
import {Text, View, Dimensions, StyleSheet} from 'react-native';
import axios from 'axios';

export default class Genres extends Component {
  constructor(props) {
    super(props);
    this.state = {
      genres: [],
    };
  }

// Fetching data genres  
  componentDidMount = async () => {
    const url = 'http://code.aldipee.com/api/v1/movies/';
    const getMovies = await axios.get(url + this.props.id);
    this.setState({genres: getMovies.data.genres});
  };

  render() {
    return (
      <View
        style={Styles.formatGenre}>
        <Text>
          {this.state.genres.map(genre => (
            <Text
              key={genre.id}
              style={{
                color: '#fff',
                fontFamily: 'Nunito-Light',
              }}>
              {genre.name} {'  '}
            </Text>
          ))}
        </Text>
      </View>
    );
  }
}

const winWidth = Dimensions.get('window').width;
const Styles = StyleSheet.create({
  formatGenre:{
    flex: 1,
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    width: winWidth * 0.6,
    alignItems: 'center',
  }
})